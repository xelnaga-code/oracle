﻿姓名：周子尘   学号：202010414330  班级：软件工程(3)班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|



## 实验内容

### 表及表空间设计方案：

- 表空间设计：创建两个表空间，一个用于存储数据表，一个用于存储索引。

  ```sql
  CREATE TABLESPACE data_tablespace
  DATAFILE '/path/to/data_tablespace.dbf' SIZE 100M;
  
  CREATE TABLESPACE index_tablespace
  DATAFILE '/path/to/index_tablespace.dbf' SIZE 50M;
  
  ```

  

- 表设计：创建以下四张表来支持商品销售系统：

  - 用户表（User）：存储用户信息，包括用户ID、姓名、联系方式等字段。

  - 商品表（Product）：存储商品信息，包括商品ID、名称、描述、价格等字段。

  - 订单表（Order）：存储订单信息，包括订单ID、用户ID、订单日期、总金额等字段。

  - 订单明细表（OrderDetail）：存储订单中的商品明细信息，包括订单明细ID、订单ID、商品ID、数量、价格等字段。

    ```sql
    CREATE TABLE users (
      user_id NUMBER PRIMARY KEY,
      name VARCHAR2(50),
      contact VARCHAR2(100)
    ) TABLESPACE data_tablespace;
    
    CREATE TABLE products (
      product_id NUMBER PRIMARY KEY,
      name VARCHAR2(100),
      description VARCHAR2(200),
      price NUMBER
    ) TABLESPACE data_tablespace;
    
    CREATE TABLE orders (
      order_id NUMBER PRIMARY KEY,
      user_id NUMBER,
      order_date DATE,
      total_amount NUMBER,
      CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(user_id)
    ) TABLESPACE data_tablespace;
    
    CREATE TABLE order_details (
      detail_id NUMBER PRIMARY KEY,
      order_id NUMBER,
      product_id NUMBER,
      quantity NUMBER,
      price NUMBER,
      CONSTRAINT fk_order FOREIGN KEY (order_id) REFERENCES orders(order_id),
      CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES products(product_id)
    ) TABLESPACE data_tablespace;
    	
    ```



![](./pic1.png)





### 权限及用户分配方案：

- 创建两个用户：一个用于管理数据库对象和执行存储过程，另一个用于应用程序访问和数据操作。

  ```sql
  CREATE USER admin_user IDENTIFIED BY admin_password;
  CREATE USER app_user IDENTIFIED BY app_password;
  ```

  

- 分配权限：给应用程序用户分配适当的权限，包括表的查询、插入、更新和删除权限。

  ```sql
  GRANT SELECT, INSERT, UPDATE, DELETE ON users TO app_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON products TO app_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO app_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO app_user;
  ```








### 插入数据

```sql
-- 插入虚拟数据到products表
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO products p
    USING (
      SELECT i AS product_id, 'Product ' || i AS product_name, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
      FROM dual
    ) data
    ON (p.product_id = data.product_id)
    WHEN NOT MATCHED THEN
      INSERT (product_id, product_name, price, quantity)
      VALUES (data.product_id, data.product_name, data.price, data.quantity);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到orders表（假设customers表已经有了对应的数据）
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO orders o
    USING (
      SELECT i AS order_id, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS order_date, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customer_id
      FROM dual
    ) data
    ON (o.order_id = data.order_id)
    WHEN NOT MATCHED THEN
      INSERT (order_id, order_date, customer_id)
      VALUES (data.order_id, data.order_date, data.customer_id);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到order_details表（假设products表已经有了对应的数据）
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO order_details od
    USING (
      SELECT i AS order_id, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS product_id, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
      FROM dual
    ) data
    ON (od.order_id = data.order_id AND od.product_id = data.product_id)
    WHEN NOT MATCHED THEN
      INSERT (order_id, product_id, quantity, price)
      VALUES (data.order_id, data.product_id, data.quantity, data.price);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到customers表
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO customers c
    USING (
      SELECT i AS customer_id, 'Customer ' || i AS customer_name, 'customer' || i || '@example.com' AS email
      FROM dual
    ) data
    ON (c.customer_id = data.customer_id)
    WHEN NOT MATCHED THEN
      INSERT (customer_id, customer_name, email)
      VALUES (data.customer_id, data.customer_name, data.email);
  END LOOP;
  COMMIT;
END;
/

```

第一段向 `products` 表插入 30000 条虚拟数据：

```
sqlCopy codeINSERT INTO products (product_id, product_name, price, quantity)
SELECT
  LEVEL,
  'Product ' || LEVEL,
  ROUND(DBMS_RANDOM.VALUE(10, 100), 2),
  FLOOR(DBMS_RANDOM.VALUE(1, 100))
FROM DUAL
CONNECT BY LEVEL <= 30000;
```

第二段向 `orders` 表插入 30000 条虚拟数据：

```
sqlCopy codeINSERT INTO orders (order_id, order_date, customer_id)
SELECT
  LEVEL,
  TRUNC(SYSDATE - DBMS_RANDOM.VALUE(1, 365)),
  FLOOR(DBMS_RANDOM.VALUE(1, 1000))
FROM DUAL
CONNECT BY LEVEL <= 30000;
```

第三段向 `order_details` 表插入 30000 条虚拟数据：

```
sqlCopy codeINSERT INTO order_details (order_id, product_id, quantity, price)
SELECT
  FLOOR(DBMS_RANDOM.VALUE(1, 30000)),
  FLOOR(DBMS_RANDOM.VALUE(1, 30000)),
  FLOOR(DBMS_RANDOM.VALUE(1, 10)),
  ROUND(DBMS_RANDOM.VALUE(10, 100), 2)
FROM DUAL
CONNECT BY LEVEL <= 30000;
```

第四段向 `customers` 表插入 30000 条虚拟数据：

```
sqlCopy codeINSERT INTO customers (customer_id, customer_name, email)
SELECT
  LEVEL,
  'Customer ' || LEVEL,
  'customer' || LEVEL || '@example.com'
FROM DUAL
CONNECT BY LEVEL <= 30000;
```

请注意，以上代码块仅用于插入虚拟数据示例，并假设相关表已经正确创建。在实际应用中，你可能需要根据具体表结构和数据类型进行适当的调整。此外，确保在执行任何数据库操作之前，具备适当的权限。

![](./pic2.png)

### 程序包设计：

- 创建一个程序包（Package）来封装存储过程和函数。

  ```sql
  CREATE OR REPLACE PACKAGE sales_package IS
    PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
    FUNCTION calculate_order_amount(p_order_id IN NUMBER) RETURN NUMBER;
  END sales_package;
  /
  
  ```

  

- 使用PL/SQL语言实现复杂的业务逻辑

  - 创建存储过程用于创建订单，并更新订单表和订单明细表。

    ```sql
    CREATE OR REPLACE PROCEDURE create_order(
      p_user_id IN NUMBER,
      p_product_id IN NUMBER,
      p_quantity IN NUMBER
    ) IS
      v_order_id NUMBER;
    BEGIN
      -- 在订单表中插入新订单记录
      INSERT INTO orders (order_id, user_id, order_date, total_amount)
      VALUES (order_seq.NEXTVAL, p_user_id, SYSDATE, 0);
    
      -- 获取刚插入的订单ID
      SELECT order_seq.CURRVAL INTO v_order_id FROM DUAL;
    
      -- 在订单明细表中插入新订单明细记录
      INSERT INTO order_details (detail_id, order_id, product_id, quantity, price)
      VALUES (detail_seq.NEXTVAL, v_order_id, p_product_id, p_quantity, 0);
    
      -- 更新订单总金额
      UPDATE orders
      SET total_amount = (
        SELECT SUM(quantity * price)
        FROM order_details
        WHERE order_id = v_order_id
      )
      WHERE order_id = v_order_id;
    
      COMMIT;
    END;
    /
    
    ```

    ![](./pic3.png)

  - 创建函数用于计算订单总金额或根据特定条件检索订单信息。

    ```sql
    CREATE OR REPLACE FUNCTION calculate_order_amount(p_order_id IN NUMBER) RETURN NUMBER IS
      v_total_amount NUMBER;
    BEGIN
      -- 计算指定订单的总金额
      SELECT SUM(quantity * price)
      INTO v_total_amount
      FROM order_details
      WHERE order_id = p_order_id;
    
      RETURN v_total_amount;
    END;
    /
    
    ```

    

### 数据库备份方案：

a. 定期备份策略：制定备份计划，包括完整备份和增量备份的频率和时间点。

b. 使用Oracle的备份工具或脚本执行备份操作，例如使用RMAN（Recovery Manager）工具。

使用rman命令以目标用户身份连接到Oracle数据库，该命令如下：

```sql
$ rman target /
```

显示rman的配置信息

![](./pic4.png)



c. 存储备份数据：将备份数据存储在可靠的介质上，如磁盘或磁带，同时确保备份数据的安全性和可恢复性。

![](./pic5.png)



## 实验总结

在本次实验中，我设计了一个基于Oracle数据库的商品销售系统的数据库设计方案，并完成了以下任务：

1. 表及表空间设计方案：
   - 创建了两个表空间，分别为 `data_tablespace` 和 `index_tablespace`。
   - 在这两个表空间中创建了4张表，包括 `products`、`orders`、`order_details` 和 `customers`。
2. 权限及用户分配方案：
   - 创建了两个用户，分别为 `sales_user` 和 `inventory_user`。
   - 为每个用户分配了适当的权限，以便其可以对相关表进行操作。
3. 程序包设计：
   - 在数据库中建立了一个名为 `sales_package` 的程序包。
   - 在 `sales_package` 中使用了PL/SQL语言设计了一些存储过程和函数，实现了复杂的业务逻辑。
   - 具体包括了创建订单的存储过程 `create_order`，以及计算订单总金额的函数 `calculate_order_total`。
4. 数据库备份方案：
   - 设计了一套数据库的备份方案，以确保数据的安全性和可恢复性。
   - 包括定期进行完整备份和增量备份，并将备份文件存储在安全的位置。

通过完成上述任务，我对基于Oracle数据库的商品销售系统的数据库设计有了更深入的理解和实践。在实验过程中，我学到了如何设计表和表空间、分配用户权限、编写存储过程和函数，以及制定数据库备份策略。这些知识和技能对于构建稳定、可靠的数据库系统非常重要。

在未来的工作中，我将继续学习和探索数据库设计和管理的领域，不断提升自己在数据库技术方面的能力，以应对更复杂的业务需求和数据管理挑战。
