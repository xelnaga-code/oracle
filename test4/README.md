# 实验4：PL/SQL语言打印杨辉三角

姓名：周子尘   学号：202010414330  班级：软件工程(3)班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

### 运行代码

![](./pic1.png)



## 创建存储过程

### 代码展示

```sql
create or replace PROCEDURE YHTriangle(N IN integer)
AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --打印第1行
    dbms_output.put(rpad(1,9,' '));--第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;

```

创建效果:

![](./pic2.png)

![](./pic3.png)

## 运行存储过程以打印杨辉三角

```sql
SET SERVEROUTPUT ON;
EXECUTE YHTriangle(10);
```

![](./pic4.png)

## 实验结论

​	本次实验主要是掌握Oracle PL/SQL语言的应用和存储过程的编写，通过练习杨辉三角的代码，将其转化为一个存储过程，并实现可接受行数N作为参数的功能。

​	在实验过程中，我深刻认识到PL/SQL语言是一种非常强大的编程语言，可以与Oracle数据库结合使用，实现复杂的数据处理和业务逻辑。存储过程是PL/SQL语言的一个重要组成部分，可以提高程序的可重用性和可维护性，同时也可以提高程序的执行效率和安全性。

​	通过本次实验，我了解了存储过程的创建过程，其中包括定义存储过程的名称、参数和过程体，并且了解了如何使用SQL语句来创建存储过程。在创建存储过程时，需要注意参数的数据类型、过程体中的逻辑和异常处理等方面，这些都是保证存储过程正常运行的重要因素。

​	最后，通过将杨辉三角代码转换为存储过程的实践，我深刻认识到PL/SQL语言和存储过程的强大功能和应用价值。在今后的数据库编程和开发中，我将更加深入地学习和运用这些知识，为数据处理和业务逻辑的实现提供更加高效和优秀的方案。
