# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验过程

### 创建一个包，命名为MYPACK

![](./pic1.png)

### 在包中创建所需函数与过程

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](./pic2.png)

## 测试

### 函数Get_SalaryAmount()测试

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

输出:

![](./pic3.png)

### 过程Get_Employees()测试

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```

输出：
![](./pic4.png)



## 实验结论

​	本次实验主要学习了PL/SQL语言的结构、变量和常量的声明和使用方法，以及包、过程和函数的用法。通过实验的练习，我深刻认识到PL/SQL语言是一种用于Oracle数据库的编程语言，可以实现复杂的数据处理和业务逻辑。以下是我对本次实验的总结：

1.PL/SQL语言结构

​	PL/SQL语言结构与其他编程语言类似，包括变量、常量、数据类型、条件语句、循环语句、异常处理等。学习这些语言结构可以帮助我们更好地理解和使用PL/SQL语言。

2.变量和常量的声明和使用方法

​	在PL/SQL中，可以声明变量和常量，并对它们进行赋值。变量和常量可以使用不同的数据类型，如数值型、字符型、日期型等。在编写PL/SQL程序时，变量和常量的使用是非常重要的，可以提高程序的灵活性和可维护性。

3.包、过程和函数的用法

​	包、过程和函数是PL/SQL中的重要组成部分，可以将程序逻辑封装在一个独立的单元中，方便程序的调用和维护。包是一个逻辑单元，可以包含一组相关的过程和函数，方便程序的组织和管理。过程是一个独立的子程序，可以接收参数并执行一系列操作。函数与过程类似，但是必须返回一个值。

​	在本次实验中，我们通过创建一个包和其中的一个函数和一个过程，学习了包、过程和函数的用法。这些程序单元可以提高程序的可重用性和可维护性，同时也可以提高程序的执行效率和安全性。

总之，本次实验让我对PL/SQL语言的应用有了更深入的理解，对于今后在数据库编程和开发中会有很大的帮助。
