姓名：周子尘   学号：202010414330

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

 	对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

### 参考

用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：
权限分配过程如下

$ sqlplus system/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr;



## 实验过程

首先以system身份登录到pdborcl，然后授权。

```
[oracle@oracle1 ~]$ sqlplus system/123@localhost/pdborcl
```

```
GRANT select_catalog_role TO hr;
```

授权成功。

```
 GRANT SELECT ANY DICTIONARY TO hr;
```

授权成功。

```
 GRANT ADVISOR TO hr;
```

授权成功。



查询employee里面，工资在10000以下并且部门是 'IT'和'Sales' 的部门名字和平均工资

### 查询1

```
set autotrace on
select d.department_name,avg(e.salary) 
from hr.departments d,hr.employees e 
where d.department_id = e.department_id
and d.department_name in ('IT','Sales') and e.salary<10000
group by d.department_name ;
```



```
DEPARTMENT_NAME 	       AVG(E.SALARY)

IT					5760
Sales				  7847.82609

执行计划
Plan hash value: 3808327043


| Id  | Operation		      | Name		  | Rows  | Bytes | Cost
(%CPU)| Time	  |


|   0 | SELECT STATEMENT	      | 		  |	1 |    23 |
5  (20)| 00:00:01 |
|   1 |  HASH GROUP BY		      | 		  |	1 |    23 |
5  (20)| 00:00:01 |
|   2 |   NESTED LOOPS		      | 		  |	7 |   161 |
4   (0)| 00:00:01 |
|   3 |    NESTED LOOPS 	      | 		  |    20 |   161 |
4   (0)| 00:00:01 |
|*  4 |     TABLE ACCESS FULL	      | DEPARTMENTS	  |	2 |    32 |
3   (0)| 00:00:01 |
|*  5 |     INDEX RANGE SCAN	      | EMP_DEPARTMENT_IX |    10 |	  |
0   (0)| 00:00:01 |
|*  6 |    TABLE ACCESS BY INDEX ROWID| EMPLOYEES	  |	3 |    21 |
1   (0)| 00:00:01 |



Predicate Information (identified by operation id):
4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
6 - filter("E"."SALARY"<10000)

Note

this is an adaptive plan
统计信息
58  recursive calls
0  db block gets
22  consistent gets
1  physical reads
0  redo size
733  bytes sent via SQLNet to client
608  bytes received via SQLNet from client
2  SQL*Net roundtrips to/from client
0  sorts (memory)
0  sorts (disk)
2  rows processed

sqldever用时0.009s
```

分析：

​		该查询语句通过员工表employees和部门表departments来查询部门的总人数和平均工资，并按照部门名’IT’和’Sales’进行分组查询。总人数直接使用了count(员工表id)得到员工人数，平均工资使用avg(员工表salary)算出。使用了多表联查从部门中找出了目标部门然后再通过其department_id在员工表中找出该部门所有的员工。

###### **优化：**

​		通过创建多个索引改进此语句的执行计划，也可以考虑改进物理方案设计的访问指导或者创建推荐的索引。原理是创建推荐的索引可以显著地改进此语句的执行计划。但由于使用典型的SQL工作量运行“访问指导”可能比单个语句更加可取，所以通过这种方法可以获得全面的索引建议，包括计算索引维护的开销、附加的空间消耗。


查询2

```
set autotrace on
select d.department_name,avg(e.salary) 
from hr.departments d,hr.employees e 
where d.department_id = e.department_id
and d.department_name = 'IT' and e.salary<10000 or d.department_name= 'Sales' 
group by d.department_name ;
```



```
DEPARTMENT_NAME 	       AVG(E.SALARY)

IT					5760
Sales				  6461.83178

执行计划
Plan hash value: 2646959616


| Id  | Operation			| Name		    | Rows  | Bytes | Co
st (%CPU)| Time     |


|   0 | SELECT STATEMENT		|		    |	110 |  3300 |
11  (10)| 00:00:01 |
|   1 |  HASH GROUP BY			|		    |	110 |  3300 |
11  (10)| 00:00:01 |
|   2 |   VIEW				| VW_ORE_19FF4E3E   |	110 |  3300 |
10   (0)| 00:00:01 |
|   3 |    UNION-ALL			|		    |	    |	    |
|	    |
|   4 |     MERGE JOIN CARTESIAN	|		    |	107 |  1712 |
6   (0)| 00:00:01 |
|*  5 |      TABLE ACCESS FULL		| DEPARTMENTS	    |	  1 |	 12 |
3   (0)| 00:00:01 |
|   6 |      BUFFER SORT		|		    |	107 |	428 |
3   (0)| 00:00:01 |
|   7 |       TABLE ACCESS FULL 	| EMPLOYEES	    |	107 |	428 |
3   (0)| 00:00:01 |
|   8 |     NESTED LOOPS		|		    |	  3 |	 69 |
4   (0)| 00:00:01 |
|   9 |      NESTED LOOPS		|		    |	 10 |	 69 |
4   (0)| 00:00:01 |
|* 10 |       TABLE ACCESS FULL 	| DEPARTMENTS	    |	  1 |	 16 |
3   (0)| 00:00:01 |
|* 11 |       INDEX RANGE SCAN		| EMP_DEPARTMENT_IX |	 10 |	    |
0   (0)| 00:00:01 |
|* 12 |      TABLE ACCESS BY INDEX ROWID| EMPLOYEES	    |	  3 |	 21 |
1   (0)| 00:00:01 |



Predicate Information (identified by operation id):
5 - filter("D"."DEPARTMENT_NAME"='Sales')
10 - filter("D"."DEPARTMENT_NAME"='IT' AND LNNVL("D"."DEPARTMENT_NAME"='Sales'
))
11 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
12 - filter("E"."SALARY"<10000)

Note

this is an adaptive plan


统计信息

 22  recursive calls
  0  db block gets
 29  consistent gets
  0  physical reads
  0  redo size
733  bytes sent via SQL*Net to client
608  bytes received via SQL*Net from client
  2  SQL*Net roundtrips to/from client
  1  sorts (memory)
  0  sorts (disk)
  2  rows processed


sqldever用时0.006s；
```

分析：

​	该查询语句同样是通过员工表employees和部门表departments来查询部门的总人数和平均工资，并按照部门名’IT’和’Sales’进行分组查询。判断部门ID和员工ID是否对应，由having确认部门名字是IT和sales来查询部门总人数和平均工资。

###### **优化**：sqldeveloper并未给出优化建议



## 结果分析

总的来看，查询1比查询2更优，这是因为查询1除了"consistent gets = 10"比查询2的"consistent gets = 9"稍差，其他参数都优于查询2.从分析两个SQL语句可以看出，查询1是先过滤后汇总(where子句)。参与汇总与计算的数据量少。而查询2是先汇总出后过滤(having子句)，参与汇总与计算的数据量多。



